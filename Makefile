.PHONY: build
build: housecat/housecat cmark/cmark
	./build.sh

.PHONY: dev
dev:
	dev-refresh -s public -c make plugins content theme imgs

.PHONY: webhook-server
webhook-server:
	node webhook-server 51207 $(TOKEN)

cmark/cmark: cmark/.git
	mkdir -p cmark/build
	cd cmark/build && cmake ..
	make -C cmark/build
	mv cmark/build/src/cmark cmark

housecat/housecat: housecat/.git
	make -C housecat

%/.git:
	git submodule init $*
	git submodule update $*
