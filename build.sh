#!/bin/sh

set -e

cmark=./cmark/cmark
housecat=./housecat/housecat

build()
{
	for F in $(ls "$1"); do
		if [ -d "$1/$F" ]; then
			mkdir -p "$2/$F"
			build "$1/$F" "$2/$F"
		elif echo "$F" | grep '.md$' >/dev/null; then
			html=$(echo "$F" | sed 's/\.md$/.html/')
			$cmark < "$1/$F" > "$2/$html"
		else
			cp "$1/$F" "$2/$F"
		fi
	done
}

echo "Building..."
rm -rf input
build "content" "input"

$housecat .
echo "Done."
