<!--date: 21 Apr 2021 12:00 GMT-->

# PSA: Nextcloud is broken in Ubuntu 21.04

Date: 2021-04-21 \
Git: <https://gitlab.com/mort96/blog/blob/published/content/00003-blurbs/00000-nextcloud-borked.md>

The Nextcloud desktop sync client on Ubuntu 21.04 crashes on start-up.
This started around the 8th of April in the Ubuntu 21.04 beta
([launchpad bug](https://bugs.launchpad.net/ubuntu/+source/nextcloud-desktop/+bug/1923053)),
and it hasn't been fixed yet. As I'm writing this, Ubuntu 21.04 is scheduled to be
released tomorrow, and Ubuntu's [final freeze](https://wiki.ubuntu.com/FinalFreeze)
was a week ago, so I'm guessing that this bug will slip through and affect
a lot of Nextcloud users.

![The nextcloud program segfaults on start-up](/_/imgs/nextcloud-borked-segfault.png)

As a workaround, you can stay on an older Ubuntu release until the bug is fixed
(or longer; 20.04 is a nice stable Ubuntu release).
But if you want to upgrade to 21.04, the easiest way to keep Nextcloud working
is to install the Nextcloud AppImage from <https://nextcloud.com/install/#install-clients>.

To install it, download the AppImage file (`Nextcloud-3.2.0-x86_64.AppImage`) from
that website, then right click the files in the file browser, open Properties,
click Permissions, and check "Allow executing file as program".

![Right click, then Properties](/_/imgs/nextcloud-borked-1.png)

![Permissions, then Allow executing as a program](/_/imgs/nextcloud-borked-2.png)


Alternatively, you can run this from the command line:

``` shell
chmod +x Downloads/Nextcloud-3.2.0-x86-64.AppImage
```

You can move the AppImage to somewhere more appropriate, such as
your desktop or Documents folder, then double-click it to run.
In my experience, it picks up your existing nextcloud and Just Works™.
However, it won't be as well integrated with the system as the normal
`nextcloud-desktop` package is. It won't be available through the Applications
menu or the launcher, and you have to keep track of that nextcloud AppImage file.
It will also be a bit slower to launch than normal.

I was afraid that it wouldn't run on start-up, but that seems to work just fine.

Once Ubuntu's `nextcloud-desktop` package is updated to fix the crashing issues,
you can just delete the `Nextcloud-3.2.0-x86_64.AppImage` file and go back to
using Ubuntu's version.

If you want to integrate the AppImage version of the Nextcloud desktop client
with your system, the official way to do that
(according to [the FAQ](https://docs.appimage.org/user-guide/faq.html#id3))
is to use the [AppImage daemon](https://github.com/probonopd/go-appimage).
I wouldn't bother with that for a temporary workaround, but it might be interesting
to you if you want to keep using the AppImage instead of Ubuntu's package.

---

You might've thought that the `nextcloud-desktop` package from
[the official Nextcloud development team PPA](https://launchpad.net/~nextcloud-devs/+archive/ubuntu/client)
would work, but it doesn't seem to. I got the same segfault-at-launch issue
with the client from the PPA.
