# DRAFT: GDB integration in Snow

<div class="social">
	<a href="https://news.ycombinator.com/blah"><img src="/_/imgs/social-hacker-news.png"></a>
	<a href="https://reddit.com/r/C_Programming/blah"><img src="/_/imgs/social-reddit.png"></a>
	<a href="https://lobste.rs/blah"><img src="/_/imgs/social-lobsters.png"></a>
</div>

Date: 2018-10-24 \
Git: <https://gitlab.com/mort96/blog/blob/published/content/00000-home/00012-snow-gdb-integration.md>

My main non-work project lately has been Snow, a unit testing framework for C.
(My last couple of blog posts have also been tangentially related to the
project, like
[Obscure C features](https://mort.coffee/home/obscure-c-features/) and
[C compiler quirks](https://mort.coffee/home/c-compiler-quirks/).)

This isn't a long-ish post like my last few posts, it's just a short post to
show off something I think is pretty cool. I added feature to Snow to break
before each failing test case, letting you step through the code and debug the
failing test with GDB. It's not an extremely complicated feature, but I haven't
seen it in testing frameworks before, and I think it could be very useful.

Here's a short demo:

<asciinema-player src="/_/imgs/gdb-integration-1.cast"></asciinema-player>

(If that doesn't run, you can download <https://mort.coffee/_/imgs/gdb-integration-1.cast> and run
it with asciinema.)

The heavy lifting is done by a new `--rerun-failed` option, which makes Snow
run all tests as normal, then, once an assertion fails, it calls an empty
function (`snow_break()`) before running the test case again. Actually
re-running the test case is a bit complex, because the code longjmps all over
the place; all `defer`s from the test case has to run, then the `after_each`
block has to run again, then the `before_each` block has to run again. Only
after that can it jump back to the failing test case and call `snow_break()`.
([Here is the relevant code](https://github.com/mortie/snow/blob/837d97f771391986d42a035ddabc84d72361ac47/snow/snow.h#L690-L705),
in case you're interested in reading through a structured mess of
longjmp/setjmp.)

In order to be able to break again when the failing assert is encountered the
second time, another empty function (`snow_rerun_failed()`) is called when an
assertion fails while re-running.

The actual `--gdb` option
[is comparatively straightforward](https://github.com/mortie/snow/blob/837d97f771391986d42a035ddabc84d72361ac47/snow/snow.h#L933-L1025),
but uses some pretty cool GDB features.
GDB has a command called "commands", which lets you specify a list of commands
to run once a particular breakpoint is hit. Snow uses this with the `snow_break`
breakpoint to immediately step out of the `snow_break` function and into the
first line of the test case. It's also used with the `snow_rerun_failed`
function, to print a diagnostic message and a stack trace when an assert fails
while re-running the test case.
