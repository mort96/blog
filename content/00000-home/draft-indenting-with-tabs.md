# DRAFT: A quick introduction to indentation with tabs

Date: xxxx-xx-xx

This post is intended as a short guide to how to properly indent with tabs.
It is not intended to be a discussion about whether tabs or spaces are best,
though reading about some of the particular challenges with indenting with
tabs, and how they can be ovecome, could help you decide for yourself.

First, some mythbusting: When talking about tabs vs spaces, we're not talking
about pressing the tab key or the space bar on the keyboard, but rather what
character actually ends up in the text file; a TAB character (ASCII code 9) or
a number of space characters (ASCII code 32). Even if it feels like you're
indenting with tabs, most IDEs and text editors insert 4 or so spaces when you
press tab, and delete 4 or so spaces when you press backspace (the exact number
depends on your configuration). I partially blame the (otherwise great) TV show
Silicon Valley for this misconception.

The primary goal when indenting with tabs, and the reason you might consider
using tabs yourself, is that anyone should be able to set their editor's tab
width to whatever number they prefer, and still be able to edit and read your
source code without issues. Multiple people should be able to edit the same
source files with different tab width settings without issues. The reason I'm
writing this post is that I've seen too much code which fails this goal; where
you have to set your editor's tab width setting to the same as the author's for
the file to look okay.

## Visualization

When indenting with tabs, each level of indentaion is one TAB character. I will
visualize one tab as `>———`, and one space as `·`.

This uses tabs:

``` JavaScript
if (foo) {
>———if (bar)
>———>———baz();
>———return 10;
}
```

This uses spaces:

``` JavaScript
if (foo) {
····if (bar)
········baz();
····return 10;
}
```

## Alignment

I personally don't use alignment a lot (more on that in the section titled
"Do you need alignment?"), but you should still know how to align when
indenting with tabs, and what pitfalls you might fall into.

This is the naive, and **wrong**, way to align when using tabs:

``` JavaScript
>———console.log("Hello World",
>———>———>———>———"How are you today?");
```

If we change the tab width from four (`>———`) to six (`>—————`), that becomes
this mess:

``` JavaScript
>—————console.log("Hello World",
>—————>—————>—————>—————"How are you today?");
```

The **correct** way is to align with spaces is like this:

``` JavaScript
>———console.log("Hello World",
>———············"How are you today?");
```

When we again increase the tab width to six, that code is just as readable as
before:

``` JavaScript
>—————console.log("Hello World",
>—————············"How are you today?");
```

This works because we only change the width of each tab character. Since we
aligned with 12 spaces, the aligned text will always be 12 character widths
away from the last tab, regardless of the tab's width.

### Pitfalls

There's one primary pitfall with this; you can only align code at the same
level of indentation. This will break:

``` JavaScript
>———if (foo) {··················// Look how nicely aligned
>———>———console.log("Hello");···// these comments are!
>———}
```

If we increase the tab width to six again, that code will look like this:

``` JavaScript
>—————if (foo) {··················// Look how nicely aligned
>—————>—————console.log("Hello");···// these comments are!
>—————}
```

The comments are no longer aligned. There's not really any way to do this when
indenting with tabs.


## Do you need alignment?

While you should know how to align with spaces when using tabs, and especially
the pitfalls, I will propose to you that you might not need alignment, and that
it might in fact be beneficial to not align as much, even when using spaces.

### Aligned comments

Aligning comments to the right of code is generally unnecessary; I would
suggest keeping comments on their own line. Compare this code (I'm not
visualizing whitespace anymore, as it's not relevant in this example):

``` JavaScript
····if (foo) {························// We only want to print Hello World
········console.log("Hello world");···// if the `foo` variable is true
····}
```

to this code:

``` JavaScript
>———// We only want to print Hello World
>———// if the `foo` variable is true
>———if (foo) {
>———>———console.log("Hello World");
>———}
```

While some people might prefer the former aesthetically, there are a bunch of
advantages with the latter:

* If we want to change the condition, we have to realign everything with the
  first example, but don't have to change anything with the second.
* If we want to add another line of comments, that's extremely simple with the
  second example, but again requires a lot of realignment with the first.
* If you try to keep your lines reasonably short (which you should), having
  comments on the same line as your code can be annoying.

### Aligned function call arguments

Another common case for alignment is this:

``` JavaScript
>———someFunction(arg1, arg2,
>———·············arg3, arg4);
```

I'm writing code like this:

``` JavaScript
>———someFunction(
>———>———arg1, arg2,
>———>———arg3, arg4);
```

This also has a few advantages:

* If you want to change the function name, the arguments in the first example
  has to be realigned, but that's not an issue with the second example.
* An IDE's refactor feature might be clever enough to replace all calls to a
  function with a call to a renamed function, but might not be smart enough to
  automatically realign the arguments.
* If you have long function names and many arguments, alignment can be
  problematic. Compare these two code snippets (pardon the Java, but it was the
  most realistic example of long function calls I could come up with):

``` JavaScript
>———SomeClassSingletonWithLongName.getInstance().doSomethingWeird(argument_one,
>———······························································argument_two,
>———······························································argument_three,
>———······························································argument_four,
>———······························································argument_five,
>———······························································argument_six);
```

``` JavaScript
>———SomeClassSingletonWithLongName.getInstance().doSomethingWeird(
>———>———argument_one, argument_two, argument_three,
>———>———argument_four, argument_five, argument_six);
```

### Aligned if statements


