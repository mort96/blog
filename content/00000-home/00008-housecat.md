<!--date: 08 Oct 2015 12:00 GMT-->

# Housecat, my new static site generator

Date: 2015-10-08 \
Git: https://gitlab.com/mort96/blog/blob/published/content/00000-home/00008-housecat.md

This website has gone through several content management systems throughout
the times. Years ago, it was WordPress, before I switched to a basic homegrown
one written in PHP. A while after that, I switched to a static site generator
written in JavaScript, which I called Nouwell.. Now, the time has come to yet
again move, as I just completed Housecat, my new static site generator.

Nouwell, like the PHP blogging system which came before it, was designed to be
one complete solution to write and manage blog posts. It was an admin
interface and a site builder in a complete package. With Housecat, I decided
to take a different route. It's around 1500 lines of C code, compared to
Nouwell's roughly 5000 lines of javascript for node.js, PHP, and HTML/CSS/JS.
That's because its scope is so much more limited and well defined; take a
bunch of source files in a given directory structure, and create a bunch of
output files in another directory structure.

Housecat is designed to be a tool in a bigger system. It does its one thing,
and, in my opinion, it does it pretty well. Currently, I'm editing this blog
post with vim. I'm navigating and administrating articles with regular unix
directory and file utilities. I have a tiny bash script which converts my
articles from markdown to HTML before Housecat processes them. Eventually, I
even plan to make a web interface for administrating things and writing blog
posts and such, which will use Housecat as a back-end. To my understanding,
this is what the UNIX philosophy is all about.

Housecat features a rather powerful theming system, a plugin system,
pagination, drafts (start a post with the string "DRAFT:", and it'll only be
accessible through the canonical URL, not listed anywhere), and should be
compatible with any sane web server, and is, of course,
[open source](https://github.com/mortie/housecat).

Now, some of you might be wondering why anyone would ever use C to write a
static site generator. To be honest, the main reason I chose C was that I
wanted to learn it better. However, after using it for a while, it doesn't
seem like a bad choice at all. Coming mostly from javascript, it's refreshing
to have a compiler actually tell me when something's wrong, instead of just
randomly blowing up in certain situations. C certainly isn't perfect when it
comes to compiler warnings, as anyone who has ever seen the phrase
`segmentation fault (core dumped)` will tell you, however having a compiler
tell you you're wrong at all is a very nice change, and valgrind helps a lot
with those segfaults. I also think that being forced to have more control over
what I'm doing and what goes where helps; with javascript, you can generally
throw enough hacks at the problem, and it disappears for a while. That strategy
generally literally doesn't work at all in C. That isn't to say that you can't
write good code in javascript, nor that you can't write bad code in C, but I
found it nice nonetheless.
