<!--date: 26 Jan 2025 12:00 GMT-->

# Hard numbers in the Wayland vs X11 input latency discussion

<div class="social">
	<a href="https://lobste.rs/s/oxtwre/hard_numbers_wayland_vs_x11_input_latency"><img src="/_/imgs/social-lobsters.png"></a>
	<a href="https://old.reddit.com/r/linux/comments/1iajb1o/hard_numbers_in_the_wayland_vs_x11_input_latency/"><img src="/_/imgs/social-reddit.png"></a>
	<a href="https://news.ycombinator.com/item?id=42831509"><img src="/_/imgs/social-hacker-news.png"></a>
	<a href="https://fosstodon.org/@mort/113895697982879127"><img src="/_/imgs/social-mastodon.png"></a>
</div>

Date: 2025-01-26 \
Git: <https://gitlab.com/mort96/blog/blob/published/content/00000-home/00016-wayland-input-latency.md>

Yesterday, I read this blog post:
[Wayland Cursor Lag: Just give me a break already...](https://gist.github.com/generic-internet-user/e8eec46ce159571032115f6fb064523c),
where a Linux user discusses their frustration with input latency under Wayland.
They detailed their own subjective experience of the problem,
but failed to produce hard evidence.

Even though I'm a very happy user of wayland overall,
I share the blog post's author's subjective impression that there's more
cursor lag under Wayland than under X11.
In my opinion, their experiment was limited by their phone camera's 90 FPS,
which really doesn't feel like it's enough to get conclusive numbers
when we're talking about differences which are probably on the order of
single screen refresh cycles.

So I thought: hey, I have a phone with a 240 FPS camera mode,
I bet that's enough to get some conclusive results!

Spoilers: I was right.

## Experiment design

I simply pointed my phone's camera at the screen and desk using my left hand,
made sure to get the mouse cursor, the mouse and my right hand in frame,
and recorded myself repeatedly flicking the mouse with my finger.
I recorded myself flicking it 16 times under Wayland,
logged out of the GNOME Wayland session and into a GNOME X11 session,
then did the same there.

I then converted the two resulting video files into a series of JPEGs
using ffmpeg (`ffmpeg -i <input file> %04d.jpg`),
and counted from the first frame where I could see the mouse move
until the first frame where I could clearly see that the cursor had moved.

I elected to include the start and end frame;
so if I saw the mouse move in frame 1045,
then I saw the cursor move in frame 1047,
I would count that as 3 frames.

Here's an example which depicts a latency of 3 frames (requires JavaScript):

<div class="wayland-latency-carousel">

<div>

![before 1](/_/imgs/wayland-latency-before-1.jpg)
> Here's a frame from before the mouse has begun moving.

</div>
<div>

![before 2](/_/imgs/wayland-latency-before-2.jpg)
> The mouse still hasn't moved.

</div>
<div>

![mouse moving](/_/imgs/wayland-latency-moved-1.jpg)
> Here, the mouse has just about started moving,
> so I consider this "frame 1".
> The cursor hasn't moved yet.

</div>
<div>

![still moving](/_/imgs/wayland-latency-moved-2.jpg)
> The mouse moves a bit further, but the cursor still hasn't moved.
> This is "frame 2".

</div>
<div>

![cursor moved](/_/imgs/wayland-latency-cursor.jpg)
> This is the frame where the cursor starts moving.
> This is "frame 3", so I would note down this sequence
> as taking 3 frames.

</div>
</div>

Hardware details:

* Distro: Fedora Workstation 41
* GNOME version: 47
* CPU: AMD Ryzen 9 5950X
* GPU: AMD Radeon RX 7900XT
* Monitor: Gigabyte M32U (4k IPS @ 144.99, no DPI scaling)
* Mouse: Logitech G502 Lightspeed
* Camera: iPhone 15 Pro, slo-mo 240 FPS

## Limitations

The main limitations of this experiment are:

* 240 FPS still isn't *that* much. With my 144Hz screen, I have less than
  two camera frames per screen refresh. This introduces some random variance.
* Pixels don't switch instantly, so there are ambiguous frames where the cursor
  is *barely* starting to become visible in its new location.
  I decided to count the cursor as "having moved" when there is a clearly visible
  cursor in a new location on the screen, even if the pixels haven't fully lit up.
* For some reason, the video recording from my phone contains some duplicate frames.
  I don't know why this happens. I decided to interpret these duplicate frames
  as a representation of a frame's worth of time passing, so I counted them as normal.

All these factors introduce some uncertainty in the results.
However, they *should* affect Wayland and X11 equally, so with enough data,
it should all even out.

**Update:** Another caveat I should clearly point out is that there are
many other Wayland compositors out there than GNOME's, and I have not tested them.
For that matter, there are other GPU drivers out there than AMD's.
Other compositors and other GPU drivers may show different results.

## Results

Here's the data I captured:

<div class="hscroll">
<table>
<thead>
    <tr>
        <td>Variant</td>
        <td colspan=16>Video frames of latency</td>
        <td>Average</td>
        <td>Milliseconds</td>
        <td>Refreshes</td>
    </tr>
</thead>
<tbody>
    <tr>
        <td>GNOME X11</td>
        <td>5</td>
        <td>4</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>4</td>
        <td>6</td>
        <td>5</td>
        <td>1</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>3</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>4</td>
        <td>16.7</td>
        <td>2.4</td>
    </tr>
    <tr>
        <td>GNOME Wayland</td>
        <td>6</td>
        <td>5</td>
        <td>6</td>
        <td>5</td>
        <td>5</td>
        <td>6</td>
        <td>6</td>
        <td>4</td>
        <td>8</td>
        <td>6</td>
        <td>5</td>
        <td>5</td>
        <td>5</td>
        <td>6</td>
        <td>5</td>
        <td>6</td>
        <td>5.5625</td>
        <td>23.2</td>
        <td>3.3</td>
    </tr>
</tbody>
</table>
</div>

Wayland, on average, has roughly 6.5ms more cursor latency than X11 on my system.
I don't have the statistics expertise necessary to properly analyze
whether this difference is statistically significant or not,
but to my untrained eye, it looks like there's a clear and consistent difference.

Interestingly, the difference is very close to 1 full screen refresh.
I don't know whether or not that's a coincidence.

Here are the numbers in chart form:

![Latency chart](/_/imgs/wayland-latency-chart.svg)

## Conclusion

In my mind, these results are conclusive proof that there *is* a difference
in input latency between X11 and Wayland, at least with my hardware,
and that the difference is large enough that it's plausible for some people to notice.

Further testing on more varied hardware and refresh rates is necessary
to get a clear picture of how wide-spread the problem is and how large it is.
It's likely that the magnitude of the difference varies based on factors
such as which compositor is used and what the refresh rate of the screen is.

I probably won't undertake that further testing,
because this is all very time intensive work.
My goal was only to see if I could conclusively measure *some* difference.

**Update:** I want to add a note here about what this testing does *not* show.
It does not show that there's higher input latency *in general*
in Wayland compared to X11 in a way which affects, for example, games.
It is possible that this added latency is entirely cursor-specific
and that Wayland and X11 exhibit the exact same input latency
in graphical applications and games.
It is my understanding that the cursor is handled very differently from
normal graphical applications.
Further testing would be necessary to show whether Wayland has more input latency
*in games* than X11.

<!--RSS_END-->

<script>(function() {
function setupCarousel(el) {
    const pages = [];
    for (const child of el.children) {
        pages.push(child);
        child.style.display = "none";
    }

    if (pages.length == 0) {
        return;
    }

    let index = 0;
    function setIndex(newIndex) {
        if (newIndex < 0) {
            newIndex = 0;
        } else if (newIndex >= pages.length) {
            newIndex = pages.length - 1;
        }

        pages[index].style.display = "none";
        pages[newIndex].style.display = "block";
        index = newIndex;
        currentEl.innerText = (index + 1) + " / " + pages.length;
    }

    const controlsEl = document.createElement("div");
    el.insertBefore(controlsEl, el.firstChild);

    const prevEl = document.createElement("button");
    controlsEl.appendChild(prevEl);
    prevEl.innerText = "<-";
    prevEl.ariaLabel = "Previous";
    prevEl.onclick = () => setIndex(index - 1);

    const nextEl = document.createElement("button");
    controlsEl.appendChild(nextEl);
    nextEl.innerText = "->";
    nextEl.ariaLabel = "Next";
    nextEl.onclick = () => setIndex(index + 1);

    controlsEl.appendChild(document.createTextNode(" "));

    const currentEl = document.createElement("span");
    controlsEl.appendChild(currentEl);

    setIndex(0);
}

const elements = document.querySelectorAll("div.wayland-latency-carousel");
for (const el of elements) {
    setupCarousel(el);
}
})();</script>
