# DRAFT: Annoying search engines

* <https://encrypted.google.com/search?hl=en&q=makefile%20ifneq>
	* autocorrects ifneq to ifeq
	* trying to figure out makefile include deps only when not clean
* <https://duckduckgo.com/?q=php+%40+operator&t=ffab&ia=web>
	* tried to find info on PHP @ opr
	* google/ddg doesn't care about symbols (except sometimes?)
* <https://encrypted.google.com/search?num=50&safe=off&hl=en&q=ld+unrecognized+option+%22--gdb-index%22&oq=ld+unrecognized+option+%22--gdb-index%22&gs_l=serp.3...6173.10492.0.10755.3.3.0.0.0.0.90.196.3.3.0....0...1c.1.64.serp..0.0.0.euOE_v1uma4>
	* "--gdb-index" is in quote. Why does it show results at all, if it can't
	  find anything which contains that?
* <https://encrypted.google.com/search?hl=en&q=dplog%20chromium>
	* "dplog" is close enough to "dog", so I obviously mean "dog chromium"
	  because who wants to search for logging functions in the chromium source?
* <https://s.mort.coffee/d/img/scr-2018-08-15-40228.png>
	* Ignore the most important part of my search please.
* <https://s.mort.coffee/d/img/scr-2018-08-13-42921.png>
	* Ignore the most important part of my search please.
* <https://s.mort.coffee/d/img/scr-2018-08-15-47489.png>
	* DDG doesn't even bother to inform you it's ignoring you.
	* Context: webkitgtk: "Memory pressure relief: Total: res =
	  318611456/318509056/-102400, res+swap = 314232832/314130432/-102400" -
	  Could I make that happen sooner?
* <https://s.mort.coffee/d/img/scr-2018-08-30-46008.png>
	* No, "set" is not synonymous with "get".
