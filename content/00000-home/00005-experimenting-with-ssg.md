<!--date: 27 Jun 2014 12:00 GMT-->

# Experimenting with static site generation

Hi!

If you visit this blog on a regular basis, which you probably don't, you may
have noticed that it looks a bit different from how it used to look. The
overall theme is the same, but the URLs look way different, and there is no
comment section. So what happened?

A while ago, I wrote my own content management system in PHP, replacing
WordPress. That was your standard CMS, for every request, you have a script
(PHP script in this case) which generates the page dynamically, showing you
the content you requested. March this year (2014) though, I started a new
project, which works fundamentally different.

**Enter [jsSiteBuilder](https://github.com/mortie/jsSiteBuilder)**. 

jsSiteBuilder is a static site generator. That means that nothing is built on
the fly. There's a script which reads the content of a MySQL database and
generates all the required HTML files for each and every page and post.
Whenever a user requests the website, the one and only thing the web server
ever does is what web servers do best - it fetches the file and sends it back
to the user. 

The most obvious advantage with this method is performance. Both server load
and request time is brought down to the absolute minimum. It also provides
some much needed stability. Your website doesn't go down, even in the case of
an extreme disaster. Say you accidentally delete your MySQL database, or the
database host goes down, or file permissions mess up. Usually, this would take
down the website. With this CMS however, all html files will just stay there,
available for everyone to see. Your users won't notice a thing, while you can
take all the time you need to properly fix whatever issue appeared. You can
even re-run the site building script as much as you like while everything is
down; it won't delete anything.

**Not everything with jsSiteBuilder is static however**. More specifically, it
has the admin control panel you'd expect from an old-fashioned dynamic site
generator, like WordPress. That's because the admin panel is written in PHP.
This makes it easy to create, edit, and generally administer and set up the
website. All the admin interface does is to interact with the MySQL database.
Once you're done making whatever changes you want to make, you can update the
user-facing portion of the website with the push of a button. On my blog, with
my server, completely regenerating the entire site takes no more than a few
tenths of a second.
