# DRAFT: Visual Studio Code is not open source. This is important.

In recent years, Visual Studio Code has taken the world by storm.
And not without reason; it's a great
[gratis](https://en.wikipedia.org/wiki/Gratis_versus_libre#Gratis)
text editor with loads of plug-ins,
and the impact of its innovative Language Server Protocol approach to language integration
truly cannot be understated.
Plus, as Electron apps go, it's certainly one of the better ones.
It deserves a spot in the ranks of high-profile text editors for code,
alongside the likes of Vim, Notepad++, Sublime Text, Atom (may it rest in peace)
and the IntelliJ tools.

However, there seems to be a commonly held belief that Visual Studio Code is _open source_.
In fact, Microsoft has done a great job of marketing Visual Studio Code as such.
Just look at
[this earlier version of code.visualstudio.com](https://web.archive.org/web/20170109090157/https://code.visualstudio.com/):

![Free. Open source. Runs everywhere.](/_/imgs/vscode-foss-site-old.png)

> Free. Open source. Runs everywhere.

There's even a MIT-licensed repository on GitHub called
[Microsoft/vscode](https://github.com/Microsoft/vscode/),
which claims to be "where we (Microsoft) develop the Visual Studio Code"!
No wonder people think Visual Studio Code is open source.

However, if you go to
[the current version of code.visualstudio.com](http://web.archive.org/web/20231107235844/https://code.visualstudio.com/),
you will instead be met with a new tagline:

![Free. Built on open source. Runs everywhere.](/_/imgs/vscode-foss-site-new.png)

> Free. Built on open source. Runs everywhere.

The difference looks subtle, but it's actually hugely significant.

## Visual Studio Code is not open source.

The Microsoft/vscode repository is legitimate.
It really is where Microsoft develops Visual Studio Code.
The source code is truly open source, available under the MIT license.
If you submit a pull request and Microsoft merges it,
your change will be included in the next version of Visual Studio Code.
If you clone the repository and run the compile steps,
you will end up with a text editor which looks a whole lot like Visual Studio Code.
But it won't actually _be_ Visual Studio Code, but rather the editor they call "Code - OSS".

You see, the editor called "Visual Studio Code" is a proprietary Microsoft product
licensed under [the MICROSOFT VISUAL STUDIO CODE license](https://code.visualstudio.com/License/),
complete with clauses denying you the right to
"reverse engineer, decompile or disassemble the software" or to
"share, publish, rent or lease the software".
The license is decidedly not in compliance with
[The Open Source Definition](https://opensource.org/osd/),
and what's more, Microsoft almost certainly includes additional closed source tracking and telemetry
features in their distribution of Visual Studio Code.
As a user, you simply have no insight into what actually goes in to the Visual Studio Code you
download from Microsoft.

In other words, the relationship between Visual Studio Code and "Code - OSS" is more or less
the same as the relationship between Google Chrome and Chromium.

## This is important.

You may not see this as a big deal.
After all, as I mentioned before,
you can just download the "Code - OSS" source code from the Microsoft/vscode repository,
compile it yourself, and end up with a perfectly good, actually open source text editor.
There's even a project called [VSCodium](https://vscodium.com/),
which distributes convenient builds of "Code - OSS".
If you like Visual Studio Code, but want to use an open source text editor,
you can just use VSCodium, right?
