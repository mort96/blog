<!--date: 18 Dec 2015 12:00 GMT-->

# Replacing Apple TV

Date: 2015-12-18 \
Git: <https://gitlab.com/mort96/blog/blob/published/content/00000-home/00009-replacing-apple-tv.md>

For a long time now, I and my family have used an Apple TV as our media PC. Not
those [newfangled ones with third-party games and apps](http://www.apple.com/tv/),
but the older generation, those with a set of pre-installed "apps" which let
you access certain quarantines of content, such as Netflix, YouTube, iTunes,
etc.

The Apple TV worked well enough when accessing content from those sources. The
Netflix client was good, the YouTube client kind of lackluster, and the iTunes
client decent enough. There various other "apps", but those went mostly unused.
The main problem however, affecting basically everything on the platform, is
that I live in Norway; as a result, most of the time, somewhat new TV shows or
movies we want to watch simply isn't available through those sources. Often, we
needed to play video files obtained through other means. This left us two
options:

1. Find a Mac, play the video file in VLC there, mirror the screen to the Apple
   TV. This gives us various degrees of choppy frame rate, but lets us play the
   video instantly after it's downloaded, and lets us use subtitles if we so
   desire.
2. Spend around half an hour converting the video to mp4, and stream it to the
   TV with [this tool](https://github.com/unused/airstream). This gives smooth
   frame rate, but takes a while due to converting media. It also doesn't
   support subtitles.

One day, I decided I'd had enough. I found an old laptop, threw Linux on it,
connected it to the TV, and started writing code.

## Introducing MMPC

[MMPC](https://github.com/mortie/mmpc), Mort's Media PC, is the fruits of my
endevours. It's designed to be controlled from afar, with a web interface.
It's also written in a modular fashion, and I'll go through what each
module does.

### Media Streaming

<https://github.com/mortie/mmpc-media-streamer>

The media streamer module is
the most important module. When playing a movie or episode from a TV show, we
generally have a torrent containing the media file. In the past, we would
download the movie from the torrent file, and then find a way to play it on the
TV when it's done. This module however lets us instead either paste in a
torrent link, or upload a torrent file, and it'll stream that to a VLC window
which opens on the big screen. VLC also comes with a web interface, so once you
start playing a video, the browser loads the VLC web interface, and you can
control video playback from there.

The control panel, letting you paste a magnet link, youtube link, etc:

![Control Panel](/_/imgs/mmpc-1.png)

VLC playback controls:

![Playback Controls](/_/imgs/mmpc-2.png)

### Remote Desktop

<https://github.com/mortie/mmpc-remote-desktop>

Sometimes, you need more than streaming torrent files. Netflix, for example, is
very useful, whenever it has the content we want to watch, and the same applies
to various other websites. As the computer is running a full Linux distro
instead of some locked down version of iOS, it's useful to have a way to
control it directly. However, it's also annoying to have a wireless keyboard
and mouse connected to it constantly and use that. Therefore, I decided it
would be nice to be able to remote control it from the browser.

Implementing remote desktop in the browser sounded like it would be an
interesting challenge. However, it turned out to be surprisingly easy. There's
already a library out there called [jsmpg](https://github.com/phoboslab/jsmpeg)
which basically does everything I need. It has a client to stream an mpeg
stream to a canvas element, and a server to stream to the client using
websockets. The server also has an HTTP server, which you can stream video to
and have it appear in all connected clients. Ffmpeg can both record the screen,
and output to an HTTP server.

Once I had streaming video to the client working, the rest was just listening
for various events on the client (mousemove, mousedown, etc.), and send HTTP
requests to an HTTP server, which then promptly runs an xdotool command, and
voila, remote desktop.

![Remote Desktop](/_/imgs/mmpc-3.png)

### Live Wallpapers

<https://github.com/mortie/mmpc-wallpaper>

One nice thing about the Apple TV is that it can be set to display random
pictures in your photo gallery, which is very nice. However, those pictures
have to be in your [iCloud photo library](https://www.apple.com/icloud/photos/),
which is sort of problematic, considering I don't use Apple devices, and
dislike that kind of platform lock-in for something as important as photos. I
therefore moved everything from what we used to use for photos, a shared iCloud
photo stream, over to a NAS, mounted that NAS on the media PC as a webdav
volume with davfs, and wrote this module to pick a random picture every 5
seconds and set it as the wallpaper. If the random picture it picked was in
portrait orientation, it will find another portrait picture, and put them side
by side using imagemagick before setting it as the wallpaper.

## Why not Plex/Kodi/whatever?

Media PC software already exists. However, both Plex and Kodi, to my knowledge,
sort of expects you to have a media collection stored on hard drives somewhere.
They excel at letting you browse and play that media, but we rarely find
ourselves in a situation where that would be beneficial. Most of the time, we
just want to watch a movie we haven't seen before and have had no reason to
already have in a media library, and we generally decide what movie to watch
shortly before watching it. Writing the software myself lets me tailor it
specifically to our use case.

UPDATE: It has come to my attention that Kodi has some addons which let you
stream torrent files. However, even with that, there are some things Kodi
doesn't do:

* Streaming from arbitrary video services - some services, like Netflix, have
  kodi addons, but many streaming services don't have such plugins. There's no
  plugin for [daisuki](www.daisuki.net) for example. Just having a regular
  desktop with Google Chrome, and some links to websites on the desktop for
  easier access, solves this.
* Having those unobstructed dynamic live wallpapers of pictures we've taken
  whenever video isn't playing is rather nice.
* Being able to control from a laptop instead of a remote control is useful;
  remote controls get lost, laptops don't. Typing on a laptop keyboard is also
  a lot easier than with a remote control.

You could get many of the features I want by using Kodi by installing, and
maybe writing, lots of plugins, but I'm not convinced that would've been mch
easier than just writing the thousand lines of javascript this project
required.

