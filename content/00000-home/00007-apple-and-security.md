<!--date: 01 Sep 2014 12:00 GMT-->

# Apple and security: Are we back to using our favorite band as our passwords?

I have relatively recently started switching all my online account over to
using a password system where all my passwords are over 20 characters, and the
password is different for each and every account. It also contains both
numbers, lowercase characters, and uppercase characters. I should be safe,
right? Well, not quite.

A while ago, I just randomly decided to try out Apple’s “forgot password”
feature. I’m a web developer, and am sometimes curious as to how websites
implement that kind of thing, so I headed over to <http://iforgot.apple.com/>
and typed in my Apple ID. I noticed that it gave me the option to answer
security questions.

I was first greeted with this screen, asking me for my date of birth:

![apple-dob](/_/imgs/apple-security-1.png)

The date of birth is obviously not classified information, and is basically
available to anyone who knows my name and how to use Google.

Having typed in this, I get to a new page, which looks like this:

![apple-secquestions](/_/imgs/apple-security-2.png)

It asks me what my favourite band is and what my first teacher’s name is.
None of that is secret either; anyone who know me knows that my favorite band
is Metallica, and there are traces of that all throughout the Internet, and if
it’s not in public records somewhere, anyone could just randomly ask me what
my first teacher’s name was, and I’d probably answer honestly.

Anyways, typing in that information, I find something truly terrifying:

![apple-terrifying](/_/imgs/apple-security-3.png)

I was able to change my password. Only knowing my email address, my date of
birth, my favourite band, and my first teacher, anyone could take complete
control of all my Apple devices, remotely delete everything on them, access all
my images, all documents, everything. And there would be nothing I could do to
stop it. After some days, I would probably notice that I couldn’t log in to
anything, and would call tech support, but at that point, it would already
have been way too late. Anyone could by then have remotely deleted all my
data, after backing it up on their machine. Only by knowing publicly available
information about me, or asking me seemingly innocent questions via chat.

This isn’t even a case of me using terrible security questions either. Apple
only allows you to pick from a small set of security questions, and the vast
majority of them were completely inapplicable to me. I have no favourite
children’s book. I’m not sure what my dream  job is. I didn’t have a childhood
nickname, unless we count “mort”, which isn’t really a “childhood” nickname,
as it’s my current nick to this day. I don’t have a car, so I don’t know the
model of my first car. I have no favourite film star or character. Et cetera.
Those are all questions I could’ve chosen instead of “Who was your favourite
band or singer in school?”, but none are applicable to me, and more
importantly, none of them would be more secure than my current security
questions.

Is this standard for security really acceptable from anyone, much less the
world’s most valuable tech company, in this day and age? Are we really back
to the dark ages of using birth dates, favorite bands, and other personal
information as our passwords? Didn’t security experts find out that this was
a bad idea a long time ago?

There are of course ways to mitigate the effects of Apple's poorly designed
system. You could generate new random passwords for each security questions if
you're using a password manager, or you could make up fake answers. I highly
suggest going to https://appleid.apple.com/signin and change your security
questions right away. However, Apple's solution is still broken. I expect that
the vast majority of people will give their actual personal information as the
answers, because after all, that's what the website asks you to do.
