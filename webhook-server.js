let http = require("http");
let spawn = require("child_process").spawn;

if (process.argv.length != 4) {
	console.log("Usage: wh-server <port> <secret>");
	process.exit(1);
}

let port = process.argv[2];
let token = process.argv[3];
let branch = "published"

function run(cmd, ...args) {
	return new Promise((resolve, reject) => {
		console.log(cmd, ...args);
		let child = spawn(cmd, args, { stdio: "inherit" });
		child.on("exit", (code, sig) => {
			if (code === null || code !== 0)
				reject("Exited with code "+code+".");
			else
				resolve();
		});
	});
}

async function update() {
	await run("git", "fetch");
	await run("git", "checkout", branch);
	await run("git", "pull");
	await run("make");
	console.log("Successfully updated content.");
}

let server = http.createServer((req, res) => {
	let str = "";
	req	.on("data", d => {
		if (str.length + d.length > 10000) {
			console.error("Too big payload.");
			return res.end();
		};

		str += d;
	});
	req.on("end", () => {
		if (req.headers["x-gitlab-token"] !== token) {
			console.error("Got request with invalid token.");
			return res.end();
		}

		let pl;
		try {
			pl = JSON.parse(str);
		} catch (err) {
			console.error(err);
			return res.end();
		}

		if (pl.ref !== "refs/heads/"+branch) {
			console.error("Ignoring push to "+pl.ref+".");
			return res.end();
		}

		update();

		res.end();
	});
});
server.listen(port, "0.0.0.0");
