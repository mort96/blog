#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

enum op {
	OP_CONSTANT, OP_ADD, OP_PRINT, OP_INPUT, OP_DISCARD,
	OP_GET, OP_SET, OP_CMP, OP_JGT, OP_HALT,
};

union instr {
	void (*fn)(union instr *instrs, int32_t *stackptr, int32_t *input);
	int32_t operand;
};

#define POP() (*(--stackptr))
#define PUSH(val) (*(stackptr++) = (val))
#define STACK(offset) (*(stackptr - 1 - offset))
#define OPERAND() (instrs[1].operand)

static void op_constant(union instr *instrs, int32_t *stackptr, int32_t *input) {
	PUSH(OPERAND());
	instrs[2].fn(&instrs[2], stackptr, input);
}

static void op_add(union instr *instrs, int32_t *stackptr, int32_t *input) {
	int32_t b = POP();
	int32_t a = POP();
	PUSH(a + b);
	instrs[1].fn(&instrs[1], stackptr, input);
}

static void op_print(union instr *instrs, int32_t *stackptr, int32_t *input) {
	int32_t a = POP();
	printf("%i\n", (int)a);
	instrs[1].fn(&instrs[1], stackptr, input);
}

static void op_input(union instr *instrs, int32_t *stackptr, int32_t *input) {
	PUSH(*(input++));
	instrs[1].fn(&instrs[1], stackptr, input);
}

static void op_discard(union instr *instrs, int32_t *stackptr, int32_t *input) {
	POP();
	instrs[1].fn(&instrs[1], stackptr, input);
}

static void op_get(union instr *instrs, int32_t *stackptr, int32_t *input) {
	int32_t a = STACK(OPERAND());
	PUSH(a);
	instrs[2].fn(&instrs[2], stackptr, input);
}

static void op_set(union instr *instrs, int32_t *stackptr, int32_t *input) {
	int32_t a = POP();
	STACK(OPERAND()) = a;
	instrs[2].fn(&instrs[2], stackptr, input);
}

static void op_cmp(union instr *instrs, int32_t *stackptr, int32_t *input) {
	int32_t b = POP();
	int32_t a = POP();
	if (a > b) PUSH(1);
	else if (a < b) PUSH(-1);
	else PUSH(0);
	instrs[1].fn(&instrs[1], stackptr, input);
}

static void op_jgt(union instr *instrs, int32_t *stackptr, int32_t *input) {
	int32_t a = POP();
	if (a > 0) instrs += instrs[1].operand;
	else instrs += 2;
	instrs[0].fn(&instrs[0], stackptr, input);
}

static void op_halt(union instr *instrs, int32_t *stackptr, int32_t *input) {
	return;
}

int32_t i32le(unsigned char *bytes) {
	return bytes[0] << 0 | bytes[1] << 8 | bytes[2] << 16 | bytes[3] << 24;
}

union instr *preprocess(unsigned char *bytecode, size_t size) {
	size_t instr_count = 0;
	int32_t *jmptargets = (int32_t *)calloc(size, sizeof(*jmptargets));

	for (size_t i = 0; i < size;) {
		jmptargets[i] = (uint32_t)instr_count;

		enum op op = (enum op)bytecode[i];
		switch (op) {
		// 1-operand instructions
		case OP_CONSTANT:
		case OP_GET:
		case OP_SET:
		case OP_JGT:
			i += 5;
			instr_count += 2;
			break;

		// 0-operand instructions
		case OP_ADD:
		case OP_PRINT:
		case OP_INPUT:
		case OP_DISCARD:
		case OP_CMP:
		case OP_HALT:
			i += 1;
			instr_count += 1;
			break;
		}
	}

	union instr *instrs = (union instr *)calloc(instr_count, sizeof(*instrs));
	union instr *instrptr = instrs;

	void (*fns[])(union instr *instr, int32_t *stackptr, int32_t *input) = {
		op_constant, op_add, op_print, op_input, op_discard,
		op_get, op_set, op_cmp, op_jgt, op_halt,
	};

	for (size_t i = 0; i < size;) {
		enum op op = (enum op)bytecode[i];
		switch (op) {
		// 1-operand instructions
		case OP_CONSTANT:
		case OP_GET:
		case OP_SET:
			(instrptr++)->fn = fns[op];
			(instrptr++)->operand = i32le(&bytecode[i + 1]);
			i += 5;
			break;

		// 0-operand instructions
		case OP_ADD:
		case OP_PRINT:
		case OP_INPUT:
		case OP_DISCARD:
		case OP_CMP:
		case OP_HALT:
			(instrptr++)->fn = fns[op];
			i += 1;
			break;

		// Jumps
		case OP_JGT:
			{
				int32_t index = (int32_t)(instrptr - instrs);
				(instrptr++)->fn = fns[op];
				(instrptr++)->operand = (int32_t)jmptargets[i + i32le(&bytecode[i + 1])] - index;
			}
			i += 5;
			break;
		}
	}

	free(jmptargets);
	return instrs;
}
void interpret(unsigned char *bytecode, size_t size, int32_t *input) {
	int32_t stack[128];
	union instr *instrs = preprocess(bytecode, size);
	instrs[0].fn(instrs, stack, input);
	free(instrs);
}

extern unsigned char program[];
extern size_t program_size;
int main(int argc, char **argv) {
	int32_t input[] = {atoi(argv[1]), atoi(argv[2])};
	interpret(program, program_size, input);
}
