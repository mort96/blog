#include <stdlib.h>

enum op {
	OP_CONSTANT, OP_ADD, OP_PRINT, OP_INPUT, OP_DISCARD,
	OP_GET, OP_SET, OP_CMP, OP_JGT, OP_HALT,
};

unsigned char program[] = {
	OP_INPUT, OP_INPUT,

	OP_CONSTANT, 0, 0, 0, 0,

	OP_GET, 0, 0, 0, 0,
	OP_GET, 3, 0, 0, 0,
	OP_ADD,
	OP_SET, 0, 0, 0, 0,

	OP_GET, 1, 0, 0, 0,
	OP_CONSTANT, 0xff, 0xff, 0xff, 0xff, // -1 in binary (two's complement)
	OP_ADD,
	OP_SET, 1, 0, 0, 0,

	OP_GET, 1, 0, 0, 0,
	OP_CONSTANT, 0, 0, 0, 0,
	OP_CMP,
	OP_JGT, 0xd5, 0xff, 0xff, 0xff, // -43 in binary (two's complement)

	OP_GET, 0, 0, 0, 0,
	OP_PRINT,

	OP_HALT,
};

size_t program_size = sizeof(program);
