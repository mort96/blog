#include "instructions.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

void interpret(unsigned char *bytecode, int32_t *input) {
	int32_t stack[128];
	int32_t *stackptr = stack;
	unsigned char *instrptr = bytecode;

	int instrsize; // Will be initialized later

	#define POP() (*(--stackptr))
	#define PUSH(val) (*(stackptr++) = (val))
	#define STACK(offset) (*(stackptr - 1 - offset))
	#define OPERAND() ( \
		((int32_t)instrptr[1] << 0) | \
		((int32_t)instrptr[2] << 8) | \
		((int32_t)instrptr[3] << 16) | \
		((int32_t)instrptr[4] << 24))
	#define INPUT() (*(input++))
	#define PRINT(val) (printf("%i\n", (int)(val)))
	#define GOTO_RELATIVE(offset) instrptr += (offset); goto *jmptable[*instrptr]
	#define NEXT() instrptr += instrsize; goto *jmptable[*instrptr]
	#define HALT() return

	void *jmptable[] = {
#define X(name, has_operand, code...) &&case_ ## name,
#include "instructions.x"
#undef X
	};

	int32_t a, b;
	goto *jmptable[*instrptr];

#define X(name, has_operand, code...) \
	case_ ## name: \
	instrsize = has_operand ? 5 : 1; \
	code
#include "instructions.x"
#undef X
}

extern unsigned char program[];
int main(int argc, char **argv) {
	int32_t input[] = {atoi(argv[1]), atoi(argv[2])};
	interpret(program, input);
}
