#!/usr/bin/env python3

import time
import subprocess
import sys
import random

names = ['basic', 'compgoto', 'tailcall', 'compiler-pessimized-output', 'compiler-output']
args = ['1', '100000000']
runs = 80
warmups = 10
instr_count = 100000000 * 12 + 6

def time_exec(name):
    argv = ['./out/' + name] + args
    start = time.monotonic()
    subprocess.check_output(argv)
    return time.monotonic() - start

def warmup_all(names, runs):
    shuffled = [n for n in names]
    for i in range(0, runs):
        random.shuffle(shuffled)
        for n in shuffled:
            print(f"\033[2K\r{i / runs:.0%} Warmup {i + 1}/{runs}: {n}...",
                file=sys.stderr, end="")
            time_exec(n)

def time_all(names, runs):
    results = {}
    shuffled = [n for n in names]
    for n in names: results[n] = 0
    for i in range(0, runs):
        # We shuffle the order every time to eliminate any systematic advantage
        # or disadvantage from running after a particular implementation
        random.shuffle(shuffled)
        for n in shuffled:
            print(f"\033[2K\r{i / runs:.0%} Run {i + 1}/{runs}: {n}...",
                file=sys.stderr, end="")
            results[n] += time_exec(n)

    for n in names:
        results[n] /= runs

    return results

warmup_all(names, warmups)
results = time_all(names, runs)

print("\033[2K\rDone!", file=sys.stderr)
for n in results:
    secs = results[n]
    print(f"{n}: {secs * 1000:.2f}ms ({(instr_count / secs) / 1000000:.2f}M/sec)")
