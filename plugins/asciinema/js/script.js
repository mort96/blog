if (!document.querySelector("asciinema-player")) {
	return;
}

var style = document.createElement("link");
style.rel = "stylesheet";
style.href = "/_/misc/asciinema/asciinema.css";
document.getElementsByTagName("head")[0].appendChild(style);

var script = document.createElement("script");
script.src = "/_/misc/asciinema/asciinema.js";
document.body.appendChild(script);
