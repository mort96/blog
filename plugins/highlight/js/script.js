if (!document.querySelector("pre code")) {
	return;
}

var link = document.createElement("link");
link.type = "text/css";
link.rel = "stylesheet";
link.href = "/_/misc/highlight/highlight.min.css";

var script = document.createElement("script");
script.src = "/_/misc/highlight/highlight.min.js";

document.getElementsByTagName("head")[0].appendChild(link);
document.body.appendChild(script);

script.onload = function() {
	hljs.highlightAll();

	var plain = document.querySelectorAll("pre code.plain");
	for (var i in plain) {
		plain[i].className += " hljs";
	}
};
